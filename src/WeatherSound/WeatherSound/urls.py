from django.contrib import admin
from django.urls import path, include
import os

version=os.getenv("VERSION", default="1")
urlpatterns = [
    path('admin/', admin.site.urls),
    path(f'api/v{version}/', include('api.urls'))
]
