from datetime import datetime, date
from django.http import HttpResponseBadRequest, HttpResponseServerError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from drf_yasg.utils import swagger_auto_schema

from .serializers import (
    DailyWeatherSerializer,
    GetDailyWeatherSerializer,
    CitySerializer,
    CurrentWeatherSerializer,
    GetCurrentWeatherSerializer,
    GetCitySerializer,
    WeekWeatherSerializer)
from . import weathermap_wrapper
from .models import City


@swagger_auto_schema(
    method='GET',
    query_serializer=GetCurrentWeatherSerializer,
    operation_id='Retrieve local weather',
    responses={200: CurrentWeatherSerializer},
    tags=['Weather', ]
)
@api_view(['GET'])
def current_weather(request):
    queryset = request.GET
    try:
        longitude = float(queryset.get('longitude', 48.8566))
        latitude = float(queryset.get('latitude', 2.3522))
    except ValueError:
        return Response(status=HttpResponseBadRequest.status_code, data={'err': "lat and lon should be float."})
    if not (-180 <= longitude <= 180) or not (-90 <= latitude <= 90):
        return Response(status=HttpResponseBadRequest.status_code,
                        data={'err': "lat ([-90,90]) and/or lon ([-180,180]) values out of bounds."}
                        )
    data = weathermap_wrapper.get_current_weather(lon=longitude, lat=latitude)
    if data:
        return Response(data)
    return Response(status=HttpResponseServerError.status_code)


@swagger_auto_schema(
    method='GET',
    query_serializer=GetDailyWeatherSerializer,
    operation_id='Retrieve local weather',
    responses={200: DailyWeatherSerializer},
    tags=['Weather', ]
)
@api_view(['GET'])
def daily_weather(request):
    queryset = request.GET
    try:
        longitude = float(queryset.get('longitude', 48.8566))
        latitude = float(queryset.get('latitude', 2.3522))
        date_stamp = datetime.strptime(queryset.get("date"), "%Y-%m-%d").date()
    except ValueError:
        return Response(
            status=400,
            data={'err': "lat and lon should be float and date format should be YYYY-MM-DD."}
            )

    if not (-180 <= longitude <= 180) or not (-90 <= latitude <= 90) or date_stamp < date.today():
        return Response(
            status=400,
            data={'err': "lat ([-90,90]) and/or lon ([-180,180]) values out of bounds."}
            )
    data = weathermap_wrapper.get_daily_weather(longitude=longitude, latitude=latitude, date_stamp=date_stamp)
    if data:
        return Response(data)
    return Response(status=HttpResponseServerError.status_code)


@swagger_auto_schema(
    method='GET',
    query_serializer=GetCitySerializer,
    operation_id='Retrive cities',
    responses={200: CitySerializer},
    tags=['Cities', ]
)
@api_view(['GET'])
def cities(request):
    queryset = request.GET
    city_query = str(queryset.get("city_query"))
    if city_query:
        cities = City.objects.filter(name__istartswith=city_query).order_by("-inhabitant")[:10]
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data)
    return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': 'Missing parameter'})


@swagger_auto_schema(
    method='GET',
    query_serializer=GetCurrentWeatherSerializer,
    operation_id='Retrive cities',
    responses={200: WeekWeatherSerializer},
    tags=['Weather', ]
)
@api_view(['GET'])
def weekly_weather(request):
    queryset = request.GET
    try:
        longitude = float(queryset.get('longitude', 48.8566))
        latitude = float(queryset.get('latitude', 2.3522))
    except ValueError:
        return Response(status=HttpResponseBadRequest.status_code, data={'err': "lat and lon should be float."})
    if not (-180 <= longitude <= 180) or not (-90 <= latitude <= 90):
        return Response(status=HttpResponseBadRequest.status_code,
                        data={'err': "lat ([-90,90]) and/or lon ([-180,180]) values out of bounds."}
                        )
    data = weathermap_wrapper.get_weekly_weather(latitude=latitude, longitude=longitude)
    if data:
        return Response({'data': data})
    return Response(status=HttpResponseServerError.status_code)
