pipeline {
	agent any
	environment {
		/* Variables d'environnement pour le conteneur de l'application
		Nécessaires pour l'exécution des tests unitaires 
		*/
		API_KEY="7a0fdafb0d3153abfe786bebcfaa21d3" // Mon ID de connexion à l'API OpenWeatherMap ( météo )
		ALLOWED_HOSTS="localhost 127.0.0.1 0.0.0.0" // Liste des hosts autorisés à interroger l'API
        SECRET_KEY="django-insecure-)g6cpk#dx7hb!lj4m6lesena7-@zwv(xa_l8)+==5igv-ipj-i"
        DEBUG="0"

		/* Qualité de code. Seuil et règle de mesure( 70 colonnes ligne de code ) */
	    QUALITY_THRESOLD = "25"
	    MAX_LINE_LENGTH="98"
		MAX_COMPLEXITY="15"

		/* Couverture de code */
		COV_UNHEALTY="12"

        /* Seuil pour la qualité des images Docker*/
		DOCKER_QUALITY="10"

		/* Répertoire pour mon registre d'image (compte sur DockerHub) */
        DOCKER_REGISTRY="a21manga/weathersound"
		DOCKER_REGISTRY_CRED="dockerHub"

        /* Connexion à Gitlab */
		GITLAB_CRED="GitlabCred"
		GITLAB_REPO="https://gitlab.com/a_manga/jenkins_ci_project.git"
	}
	
	options {
        // This is required if you want to clean before build
        skipDefaultCheckout(true)
    }
    
	stages {
		stage ('Checkout') {
			agent any
		 	steps {
				checkout([$class: 'GitSCM', branches: [[name: '*/master']], extensions: [], 
				userRemoteConfigs: [[credentialsId: "${GITLAB_CRED}", url: "${GITLAB_REPO}"]]])
			}
		}	
		stage ('Tests') {
		    parallel {
				/* Tests de qualité de code réalisé par un conteneur Flake8 */
        		stage ('Statique') {
        		    agent {
                        dockerfile {
                          dir 'build/dockerfiles/flake8'
                          filename 'Dockerfile'
                        }
        		    }
        		    steps {
						/* Règle: 98 colonnes max, max complexité: 15
						   Je pourrais cibler mieux en excluant par exemple les fichiers hérités du framework Django  
						*/
						sh "make clean-qa"
        		        sh "flake8 src/WeatherSound --exit-zero --max-complexity $MAX_COMPLEXITY --max-line-length $MAX_LINE_LENGTH --output-file build/reports/code_quality/flake8-output.xml"
        		      
						/* Publication et définition de seuil de qualité de code */
						recordIssues qualityGates: [[threshold: QUALITY_THRESOLD, type: 'TOTAL', unstable: false]], 
						tools: [flake8(pattern: 'build/reports/code_quality/')]
        		    }
        	    }

        	    /* Tests unitaires exécutés par un conteneur unittest et coverage */
        		stage ('Dynamique'){
        		    agent {
                        dockerfile {
                            args '-e VERSION -e SECRET_KEY -e ALLOWED_HOSTS -e API_KEY -e DEBUG'
                            dir 'build/dockerfiles/unittest'
                            filename 'Dockerfile'
                        }
        		    }
        		    steps {
						/* Nettoyage du répertoire de résultat. Et exécution des tests unitaires */
        		        sh "make clean-dyn"
						sh "make prepare"
            		    sh "make test"
            		    sh "make move-reports"
            		    junit skipPublishingChecks: true, testResults: 'build/reports/unittest/unit-tests-report.xml'
                    	
						/* Couverture de code en HTML avec posssibilité d'afficher le code source (lignes non couvertes mises en évidence) */
						echo "Publish detailed HTML coverage reports"
                    	publishHTML([allowMissing: true, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/reports/coverage/htmlcov', 
						reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                    	
						/* Définition de seuils pour la couverture de code par les tests unitaires */
						echo "Publish XML coverage reports with thresolds"
        				publishCoverage adapters: [coberturaAdapter(path: 'build/reports/coverage/coverage.xml', 
						thresholds: [[thresholdTarget: 'Aggregated Report', unhealthyThreshold: COV_UNHEALTY]])],
						sourceFileResolver: sourceFiles('NEVER_STORE')
        		    }
        	    }
		    }
      	}
      	
		stage('Hadolint') {
			agent {
				docker {
					image 'hadolint/hadolint:v2.8.0-alpine'
				}
			}
			steps {
				sh "touch hadolint.json"
				/* Qualité des 2 images de tests et de l'image de l'application */
				sh "hadolint -f json build/dockerfiles/unittest/Dockerfile build/dockerfiles/flake8/Dockerfile src/Dockerfile | tee -a hadolint.json"
				recordIssues qualityGates: [[threshold: DOCKER_QUALITY, type: 'TOTAL', unstable: false]], tools: [hadoLint(pattern: 'hadolint.json')]
				sh "rm hadolint.json"
			}
		} 
		
		stage('Deploy') {
			agent any
			steps {
				script {
				    withDockerRegistry(credentialsId: "${DOCKER_REGISTRY_CRED}") {
				        docker.build("${DOCKER_REGISTRY}:${env.BUILD_ID}", "src").push()
				    }
				}
			}
		}
	}
}
