clean-qa:
	@rm -rf build/reports/code_quality
	@mkdir -p build/reports/code_quality

clean-dyn:
	@rm -rf build/reports/unittest
	@rm -rf build/reports/coverage
	@mkdir -p build/reports/unittest
	@mkdir -p build/reports/coverage

prepare:
	@cd ./src/; pipenv install --system --deploy --ignore-pipfile
	@cd ./src/WeatherSound; python manage.py collectstatic --no-input

test:
	@cd ./src/WeatherSound/; coverage run --source='.' manage.py test
	@cd ./src/WeatherSound/; coverage xml
	@cd ./src/WeatherSound/; coverage html

move-reports:
	@mv ./src/WeatherSound/unit-tests-report.xml ./build/reports/unittest/
	@mv ./src/WeatherSound/coverage.xml ./build/reports/coverage/
	@mv ./src/WeatherSound/htmlcov ./build/reports/coverage/	

#docker run --rm -ti --env-file=./.env -v ${PWD}/build/unittest:/app/output_dir unittest bash