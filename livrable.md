# Projet d'intégration et de déploiement continus avec Jenkins

## Introduction
Le projet utilisé pour ce pipeline CI/CD est est une API de prévision météo. Elle a été développée avec le framework _Django_, par la promo __MS ICD 2022__, dans le cadre d'un projet du cours portant sur les méthodes _Agile_.  
L'API fournit 4 services:
- Suggestion du nom de ville à partir des 3 premières lettres saisies dans un formulaire du _frontend_  
- La mété courante
- Les prévisions pour une journée donnée
- Les prévisions pour la semaine.  
Elle reçoit des requêtes du _frontend_ et interroge une autre API qui s'appelle __OpenWeatherMap__. Pour interroger __OpenWeatherMap__ on a besoin de créer un compte <https://openweathermap.org/> pour avoir un ID.  
J'ai mis mon l'ID de mon compte dans le Jenkinsfile (variable d'environnement `API_KEY` ). Il peut être utilisé.


## Description de l'application
### Dépôt Git ou une copie du projet
Le dépôt _Git_ du code source utilisé dans ce projet de _CI/CD_ est public. Il est disponible en lecture à cette _URL_: <https://gitlab.com/a_manga/jenkins_ci_project>.

### Quickstart de l'application avec l'image construite et enregistrée
L'API est associée à un Swagger qui permet de la tester de manière très simple. Après l'exécution du pipeline, l'image construite et poussée dans le registre peut être téléchargée et exécutée ainsi:
```bash
docker pull my_registry/my_repository/image_name:image_tag
docker run -e VERSION="1" -e API_KEY="YYYY" -e ALLOWED_HOSTS="*" -e DEBUG="1" -p XXXX:8000 my_image:image_tag

```
Il faut ensuite ouvrir en suite un navigateur et saisir l'URL `localhost:XXXX/api/v1`.  
`YYYY`: clé pour l'interrogation d'`OpenWeatherMap`. Nécessite la création d'un compte ( gratuit ) pour l'obtenir.  
`XXXX`: port local disponible.  




## Processus d’intégration
Le pocessus d'intégration comprend dans l'ordre les étapes suivantes:
- Le `Cleanup` du workspace avant le démarrage.
- Le `Checkout` du projet.
- L'exécution des tests unitaires ( tests dynamiques ) et les tests de la qualité de code ( tests statiques ). Ces deux groupes de tests sont exécutés en parallèle.
- L'exécution du test de la qualité des images _Docker_ utilisées dans le processus.
- Et enfin si ces trois premières étapes réussissent l'image du code source est contruite et enregistrées dans un registre d'images ( dans `DockerHub` ici ).


## Extensions Jenkins nécessaires à son fonctionnement et les critères de choix (1p)
- Tout d'abord j'ai accepté les `Plugins suggérés par la communauté` durant l'installation. Dans cette liste de plugins on a notamment `Git`, `Credentials Binding`, `Pipeline`, `Folders`, etc. qui sont nécessaires pour la majorité des processus Jenkins.  

Ensuite j'ai ajouté les plugins suivants:
- __Docker__, __Docker API__, __Docker Slave__, __Docker Pipeline__  
  Cet ensemble de plugins est nécessaire pour utiliser des conteneurs Docker dans la pipeline (les agents d'exécution, commandes `build` et `run`, `pull` et `push` des images, etc.).
- __Warnings Next Generation__: pour la gestion du rapport ( format _JSON_ ) de qualité de code généré par `Flake8`.   
- __HTMLPubliser__: pour la publication ( présentation ) des rapports _HTML_ ( utilisé ici pour la couverture de code ). Il donne notamment la possibilité d'afficher les fichiers et de voir précisément les lignes non couvertes ( qu'il met en évidence ).
- __Cobertura__: pour l'exploitation des rapports de couverture de code avec notamment la définition des seuils.
- __Blue Ocean__: j'ai également installé ce plugin pour avoir une meilleure présentation des stages. Il n'est pas indispensable mais il est sympathique lors pour la construction du processus ( et même après ). 


## Fichiers créés pour l’élaboration du processus
La mise en place du processus a nécessité les fichiers suivants:
- DockerCompose ( _`docker-compose.yml`_ )   
  Fichier permettant de regrouper et configurer ( images, volumes, ports, etc. ) les liens entre les différents services nécessaires au processus. Cela rend notamment possible d'exécuter une seule commande pour lancer le tout le processus ou pour l'arrêter.  
  Ici nous avons un seul service ( `Jenkins` ainsi que les ports et les volumes ( pour la persistance ) qui sont associés). 
- Makefile ( _`Makefile`_)  
  Le _makefile_ me permet d'avoir le moins possible de commandes ou de scripts dans le _Jenkinsfile_. On peut ainsi l'inclure dans la qualification de qualité du code.
- Dockerfile  
  J'ai eu besoin de trois fichiers _Dockerfile_ dans mon processus:
  - Deux fichiers ( _`build/dockerfiles/flake8/Dockerfile`_ et _`build/dockerfiles/unittest/Dockerfile`_ ) pour contruire les images des containers qui servent comme agents pour les tests unitaires et de qualité de code.
  - Un fichier ( _`src/Dockerfile`_ ) pour construire l'image du code source à enregistrer à la fin du processus. Cf. section suivante.
- Tests unitaires  
  J'ai créé un répertoire ( `build/reports` ) qui regroupe les différents rapports de tests (unitaires, qualité, et couverture de code). Ce répertoire est néttoyé et recréé au début de chaque _build_.

## Fichiers Dockerfile créés
- Agents Docker pour les tests  
Les deux fichiers ( _`build/dockerfiles/flake8/Dockerfile`_ et _`build/dockerfiles/unittest/Dockerfile`_ ) ont pour but de contruire les images des containers qui servent comme agents pour les tests unitaires et les tests de qualité de code. Ces deux images, sont élaborées à partir d'une image `Python`. En effet, le code source de l'application est écrit en `Python` avec le framework `Django`. Et il fallait notamment créer un environnement ( `pipenv` ) pour pouvoir exécuter les tests unitaires.  
  - Dockerfile `flake8`  
    Les premières lignes consistent à mettre à jour l'installateur de package `pip` et ensuite à l'utliser pour mettre en place l'environnement virtuel. Et enfin la librairie `flake8` s'installe très facilement avec juste une simple instruction.
  - Dockerfile `unittest`  
    Pour l'image du conteneur des tests unitaires, on procède de la manière et on installe deux librairies: `unittest-xml-reporting` et `coverage`. La première permet de non seulement d'exécuter les tests unitaires mais aussi de générer les résultats sous forme de fichier _XML_ compatible avec les plugins _Jenkins_ `JUnit` et `XUnit`.  
    La deuxième librairie ( `coverage` ) sert à tester la couverture de code. Elle permet également de générer un rapport de couverture sous format _XML_ et _HTML_. Les deux librairies travaillent ensemble puisqu'elles utilisent les mêmes tests unitaires et que le fichier qui sert pour la génération de rapport de couverture de code est produit pendant l'exécution des tests unitaires. D'où le choix de les regrouper dans un même agent.  
    J'aurais d'ailleurs pu regrouper les images `flake8` et `unittest` en une seul dans un seul _Dockerfile_ mais j'ai fait le choix de les séparer pour un objectif de simplicité et de légèreté des images.  

- Code source de l'application  
  C'est le fichier __Dockerfile__ ( _`src/Dockerfile`_ ) pour construire l'image du code source à enregistrer à la fin du processus. Et c'est également une image contruite à partir d'une image `Python`. Je place le _Dockerfile_ à l'endroit où se trouve les sources de l'application mais aussi les fichiers `Pipefile` et `Pipfile.lock` qui contiennent les librairies requises pour créer l'environnement virtuel dans lequel s'exécutera le conteneur. Pour le serveur d'application utilisé dans l'entrypoint, j'ai choisi `Gunicorn` qui est bien plus complet le serveur de développement qui est installé par défaut pour _Django_ ( `manage.py` ).
  
## Synthèse d'analyse des résultats du processus
### Tests unitaires
J'ai écrit __5__ tests unitaires qui tous passent. Le rapport donne juste la liste des tests avec la durée pour chaque test. Il s'agit en fait d'une toute petite API. Les __5__ tests correspondent aux __5__ services fournis par l'API. Ce qui couvrent presque déjà tout le code réellement produit ( si on exclut le code hérité du framework __Django__ ).

### Couverture de code
Pour la couverture de code je génère deux rapports:  
- `HTML Report`: Celui-ci contient non seulement les chiffres mais en plus on peut naviguer fichier par fichier et afficher les sources pour voir les lignes couvertes / non couvertes. 
- `Coverage Report`: généré à partir du rapport _XML_ et il est le résultat de l'instruction de mise en place de seuils de couverture. Il complète le rapport HTML car il permet d'avoir des détails notamment en termes de couverture par lignes, par fichier, etc.. 

Ma commande exécutée pour la couverture couvre tous les fichiers à partir de la racine du projet. J'aurais pu ajouter des filtres pour exclure les fichiers de configuration par exemple. Néamoins, de manière globale, mes pauvres ___5___ tests couvrent ___86%___ des lignes. Pas mal !

### Qualité de code (avec `Flake8` )  
J'ai mis une simple règle consistant à ce que les lignes des instructions ne pas dépassent pas __98__ colonnes ( type `E501` ) et que la complexité des instruction ne dépasse pas __15__. Et `Flake8` me génère bien un rapport ( `Flake8 Warning` ) me permettant de savoir avec précision toutes les lignes qui ne respectent pas la règle. J'ai mis un seuil de qualité à __25__ anomalies.  
Et `Flake8` me trouve __16__ lignes qui dépassent le seuil de __98__ colonnes.  Ce qui est déjà beaucoup trop ! Mais en fait, je n'ai pas exclu les répertoires et fichiers hérités du framework `Django`. 

### Qualité des images ( avec `Hadolint` )
De même `Hadolint` scanne bien les _Dockerfiles_ et donne un aperçu global. On peut également aller dans le code du Dockerfile et voir de manière précise ce qui ne va pas. Par exemple pour le Dockerfile _`build/dockerfiles/unittest/Dockerfile`_, il m'indique qu'à la ligne 6 il manque un espace. Et pour l'image _`build/dockerfiles/flake8/Dockerfile`_, il me préconise de préciser la version de _pip_ à installer ( ligne 5 ). Je trouve cela précieux comme recommandation !

### Workspace
Et enfin pour chaque build, il y a le dossier `Workspace` qui garde tout ce qui a été généré ( code source et rapports ). C'est donc également un résultat car ce contenu peut bien être utilisé pour d'autres exploitations ou analyses complémentaires.

## Conclusion
